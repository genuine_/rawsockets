
/*
 * Small p2p chat program using raw icmp sockets
 * author: Greg Lindor
 */


#include <unistd.h>
#include <time.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
 #include <netinet/ip_icmp.h>
#include <pthread.h>
#include <errno.h>

#include <stdlib.h>

#ifdef  __FreeBSD__
#include "bsdcompat.h"
#endif

//test ip 192.168.80.132 ideal port 26500

typedef int SOCKET;
#define PCK_LEN 8192
#define DATA_LEN 52  // Keep the data small, this value makes our total transmission size 98 bytes

//compile gcc -pthread filename.c -o fileexe
//global sockets, we need a recv_socket in order to spoof our sending IP address.
SOCKET sock;
SOCKET recv_sock;
//global variables
char sourceIP[15];
char destIP[15];
int src_port;
int dst_port;

// check sum for ipheader and icmp
unsigned short csum(unsigned short *buf, int nwords)
{       //
        unsigned long sum;
        for(sum=0; nwords>0; nwords--)
                sum += *buf++;
        sum = (sum >> 16) + (sum &0xffff);
        sum += (sum >> 16);
        return (unsigned short)(~sum);
}



// Server Thread to receive data
void* run_recv_thread(void* ptr){

	struct sockaddr *src = (struct sockaddr*)ptr;
	char buff[PCK_LEN];
	socklen_t len = sizeof(src);
	//This is needed because freebsd and ubuntu based distro have icmphdr defined differently. 
	//but icmp header is always 32bytes according to RFC
	int icmphdr_size = sizeof(int32_t);
	// add int32_t in order to calculate for sizeof(m_icmp->icmp_seq) + 
	// sizeof(m_icmp->icmp_id) which are both unint16_t. two shorts make a long :)
	int header_size = sizeof(struct ip) + icmphdr_size + sizeof(int32_t);

	// Just to parse out some header data.
	struct icmp *ic;

	while(1){

		int res = recvfrom(recv_sock, buff, PCK_LEN, 0, src, &len );
		ic = (struct icmp*)(buff + sizeof( struct ip));

		printf("%d : port\t %d : type\t %d : code\n", htons(ic->icmp_id ), ic->icmp_type, ic->icmp_code); 
		
		if( htons(ic->icmp_id) == dst_port  && ic->icmp_type == 0 && ic->icmp_code == 0 ){
			//this is our data, process it
			printf("We got %d bytes of data\n", res);
			//get the length of the string..MAJOR ASSUMPTION of no packet loss here
			int lendata = res - header_size;
			//move to our chatter
			char* data = buff + header_size;
			printf("Message Received: %s -- Length: %d\n",
					data, lendata );

			//reset the buffer
			memset(buff, 0, PCK_LEN);
		}
		else if( res < 0 ){
			printf("something went wrong in recvfrom()\n");
			exit(0);
		}
		continue;
	}
}

void buffer_raw_icmp( char* buffer, int buflen, char* ret_buffer,  int *lendata)
{
	char * temp = malloc(PCK_LEN);
	memset(temp, 0, PCK_LEN);

	struct ip *iph = (struct ip *) temp;
	struct icmp *m_icmp = (struct icmp *) (temp + sizeof(struct ip));
	char *data =  (char*)&m_icmp->icmp_data;
	//copy the data to send
	memcpy(data, buffer, buflen);

	//Set up the IP header structure manually.
	iph->ip_hl = 5;
	iph->ip_v = 4;
	iph->ip_tos = 16; 
	iph->ip_len = sizeof(struct ip) + sizeof(struct icmphdr) + sizeof(m_icmp->icmp_seq) + 
	sizeof(m_icmp->icmp_id) + buflen;
	iph->ip_id = htons(54321);
	iph->ip_ttl = 64; // hops
	iph->ip_p = 1; // ICMP
	// Source IP address, can use spoofed address here!!
	iph->ip_src.s_addr = inet_addr(sourceIP);
	// The destination IP address
	iph->ip_dst.s_addr = inet_addr(destIP);
	
	// Creating our fake echo request
	m_icmp->icmp_type = 0;
	m_icmp->icmp_code = 0;
	m_icmp->icmp_id = htons(dst_port); //use port as the id
	m_icmp->icmp_seq = rand()%3000+1; //between one and 3000 should be fine
	m_icmp->icmp_cksum = csum((unsigned short*)m_icmp,  sizeof(struct icmp)+ buflen );
	// Calculate the checksum for integrity
	iph->ip_sum = csum((unsigned short *)temp, sizeof(struct ip) + sizeof(struct icmp)+ buflen);
	
	// Using struct icmp here would yield the wrong size
	*lendata = (int)(sizeof(struct ip) + sizeof(struct icmphdr) + sizeof(m_icmp->icmp_seq) + 
	sizeof(m_icmp->icmp_id) + buflen);
	//printf("DEBUG: Length of data buffer is %d\n", *lendata);
	
	memcpy(ret_buffer, temp, PCK_LEN);

	free(temp);
}

int main(int argc, char* argv[])
{
	// Seed random time for our icmp sequence
	srand((unsigned int) time(0) );

	if( argc != 5 ){
		printf("Invalid Arguments!\n --- Usage is: [source host/ip] [source port] [destination host/ip] [destination port] \n");
		return -1;
	}

	//get our command line values, no checks! pass the right stuff DUDE!!
	strncpy(sourceIP,argv[1], sizeof(sourceIP));
	src_port = atoi(argv[2]);
	strncpy(destIP, argv[3], sizeof(destIP));
	dst_port = atoi(argv[4]);

	// Server Thread
	pthread_t recv_thread;
	pthread_attr_t attr;
	int thread_ret;

	int opt = 1;
	const int *optval = &opt;

	//data to send
	char data[PCK_LEN];
	memset(data, 0, PCK_LEN);

	//payload to append to header. Keep it small
	char s[DATA_LEN];

	//destination and source info
	struct sockaddr_in source, dest;

	//set up the socket
	sock = socket(PF_INET, SOCK_RAW, IPPROTO_ICMP);
	if( sock < 0 ){
		printf("Error in socket()\n");
		return -1;
	}
	recv_sock = socket(PF_INET, SOCK_RAW, IPPROTO_ICMP);
	if( sock < 0 ){
		printf("Error in socket()\n");
		return -1;
	}


	memset((void*)&source, 0, sizeof(source));
	memset((void*)&dest, 0, sizeof(dest));

	source.sin_family = PF_INET;
	dest.sin_family = PF_INET;

	// Port numbers
	source.sin_port = htons(src_port);
	dest.sin_port = htons(dst_port);

	// IP addresses
	source.sin_addr.s_addr = inet_addr(sourceIP); //this needs to be the local machine or 0.0.0.0 or else bind will fail
	dest.sin_addr.s_addr = inet_addr(destIP);

	printf("Socket is ready\n");


	//inform kernel not to fill a packet structure, as we already have our own.
	if( setsockopt(sock, IPPROTO_IP, IP_HDRINCL, optval, sizeof(opt) ) < 0 )
	{
		printf("setsocktopt() error\n");
		return -1;
	}

	//bind our receiver socket.
	int r = bind(recv_sock, (struct sockaddr*)&source, sizeof(source));
	if( r != 0){
		printf("Bind failed with %s\n", strerror(r));
		return -1;
	}

	//lets start the receiver thread
	//Prefer a detached thread, so no waiting(joins) required
	if( pthread_attr_init( &attr) != 0 ){
		printf("Failed to initialize the pthread_attr\n");
		return -1;
	}
	int ss = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	thread_ret = pthread_create( &recv_thread,&attr, run_recv_thread, (void*)&source);
	if(thread_ret != 0 ){
		printf("DEBUG: Something happened when creating the thread!\n");
		return -1;
	}

	int result;

	do{

		memset(s,0,DATA_LEN);
		printf("What would you like to say?\n");
		fgets(s, DATA_LEN, stdin);
		if( strcmp(s,"quit") == 0){
			break;
		}

		//lets construct our manual packet
		int length = 0;
		buffer_raw_icmp(s,DATA_LEN, data, &length);

		//send the data
		result = sendto(sock, data, length, 0, (struct sockaddr*)&dest, sizeof(dest) );
		if(result < 0)
		{
			printf("Error in sendto()\n");
			return -1;
		}
		printf(" Sent [ %d ] bytes of data sent\n", result);
		sleep(2);

		memset(data, 0, PCK_LEN);

	}while(1);

	pthread_attr_destroy(&attr);

	close(sock);

	return 0;
}
