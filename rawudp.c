
/*
 * Small p2p chat program using raw udp sockets
 * by: Greg Lindor
 */
 
 
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <netinet/ip_icmp.h>
#include <pthread.h>
#include <errno.h>

#include <stdlib.h>

#ifdef  __FreeBSD__
#include "bsdcompat.h"

#endif



//192.168.80.128 ideal port 26500
typedef int SOCKET;
#define PCK_LEN 8192
#define DATA_LEN 512

//compile gcc -pthread rawudp.c -o rawudp
//global socket and variables
SOCKET sock;
SOCKET recv_sock;


char sourceIP[15];
char destIP[15];
int src_port;
int dst_port;
//check sum for ipheader
unsigned short csum(unsigned short *buf, int nwords)
{       //
        unsigned long sum;
        for(sum=0; nwords>0; nwords--)
                sum += *buf++;
        sum = (sum >> 16) + (sum &0xffff);
        sum += (sum >> 16);
        return (unsigned short)(~sum);
}


void* run_recv_thread(void* ptr){

	struct sockaddr *src = (struct sockaddr*)ptr;
	char buff[PCK_LEN];
	struct udphdr *p;
	char data[DATA_LEN];
	socklen_t len = sizeof(src);
	int header_size = sizeof(struct ip) + sizeof(struct udphdr);

	while(1){

		int res = recvfrom(recv_sock, buff, PCK_LEN, 0, src, &len );
		p = (struct udphdr*)(buff + sizeof(struct ip));
        //printf("dst_port: %d  src_port: %d  hdr+data len: %d\n", htons(p->dest), htons(p->source), htons(p->len) );
		
		if( htons(p->dest) == src_port && res > header_size ){

			//printf("We got %d bytes of data\n", res);
			//printf("Parsing out the data..tink..tink..tink..\n");
			//get the length of the string..MAJOR ASSUMPTION of no packet loss here
			int lendata = res - header_size;
			//move to our chatter
			memcpy( data, (buff + sizeof(struct ip) + sizeof(struct udphdr)), lendata-1);
			//char* data = (char*)(buff + sizeof(struct ip) + sizeof(struct udphdr));
			printf("Message Received: %s\n", 
					data );
			
			//reset the buffer
			memset(data, 0, lendata);
			memset(buff, 0, PCK_LEN);
		}
		else{
			//printf("The force is not strong with this data...\n");
			continue;
		}
	}
}

//manually construct an ip/udp packet header
void buffer_raw_udp( char* buffer, int buflen, char* ret_buffer,  int *lendata)
{
    char * temp = malloc(PCK_LEN);
	memset(temp, 0, PCK_LEN);

	struct ip *iph = (struct ip *) temp;
	struct udphdr *udp = (struct udphdr *) (temp + sizeof(struct ip));
	char *data = (char*)(temp + sizeof(struct ip) + sizeof(struct udphdr));

	//copy the data to send
	memcpy(data, buffer, buflen);

	//Set up the IP header structure manually.
	iph->ip_hl = 5;
	iph->ip_v = 4;
	iph->ip_tos = 16; 
	iph->ip_len = sizeof(struct ip) + sizeof(struct udphdr) + buflen;
	iph->ip_id = htons(54321);
	iph->ip_ttl = 64; // hops
	iph->ip_p = 17; // UDP
	// Source IP address, can use spoofed address here!!!
	iph->ip_src.s_addr = inet_addr(sourceIP);
	// The destination IP address
	iph->ip_dst.s_addr = inet_addr(destIP);
	
	
	udp->source= htons(src_port);
	// Destination port number
	udp->dest = htons(dst_port);
	udp->len = htons(sizeof(struct udphdr) + buflen);
    	udp->check = csum((unsigned short*)udp,  sizeof(struct udphdr)+ buflen );
	// Calculate the checksum for integrity
	iph->ip_sum = csum((unsigned short *)temp, sizeof(struct ip) + sizeof(struct udphdr)+ buflen);
	
	*lendata = (int)(sizeof(struct ip) + sizeof(struct udphdr) + buflen);
	//printf("DEBUG: Length of data buffer is %d\n", *lendata);
	
	memcpy(ret_buffer, temp, PCK_LEN);
	//ret_buffer = temp;
	free(temp);
}

int main(int argc, char* argv[])
{
	if( argc != 5 ){
		printf("Invalid Arguments!\n --- Usage is: [source host/ip] [source port] [destination host/ip] [destination port] \n");
		return -1;
	}

	//get our command line values
	strncpy(sourceIP,argv[1], sizeof(sourceIP));
	src_port = atoi(argv[2]);
	strncpy(destIP, argv[3], sizeof(destIP));
	dst_port = atoi(argv[4]);


	pthread_t recv_thread;
	pthread_attr_t attr;
	int thread_ret;
	int opt = 1;
	const int *optval = &opt;

	//data to send
	char data[PCK_LEN];
	memset(data, 0, PCK_LEN);

	//strings to append to header
	char s[DATA_LEN];

	//destination and source info
	struct sockaddr_in source, dest, dummy;

	//set up the socket
	sock = socket(PF_INET, SOCK_RAW, IPPROTO_UDP);
	if( sock < 0 ){
		perror("Error in socket()");
		return -1;
	}
	recv_sock = socket(PF_INET, SOCK_RAW, IPPROTO_UDP);
	if( sock < 0 ){
		perror("Error in socket()");
		return -1;
	}

	int yes = 1;
	int rcvbuf = 0;

	//Set some socket options
	//inform kernel not to fill a packet structure, as we already have our own.
	if( setsockopt(sock, IPPROTO_IP, IP_HDRINCL, optval, sizeof(opt) ) < 0 )
	{
		printf("setsocktopt() error\n");
		return -1;
	}

	if( setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) < 0 )
	{
		perror("setsockopt:sock: ");
		return -1;
	}
    
    // An attempt to get rid of the ICMP responses from the router
    if( setsockopt(recv_sock, SOL_SOCKET, SO_RCVBUF, &rcvbuf, sizeof(rcvbuf)) < 0 )
    {
        perror("setsockopt for recv_socket failed: ");
        return -1;
    }
    
    if ( setsockopt(recv_sock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) < 0 )
	{
    	perror("setsockopt: ");
    	return -1;
	}


	memset((void*)&source, 0, sizeof(source));
	memset((void*)&dest, 0, sizeof(dest));
	
	source.sin_family = PF_INET;
	dest.sin_family = PF_INET;
	dummy.sin_family = PF_INET;
	// Port numbers
	source.sin_port = htons(src_port);
	dummy.sin_port = htons(src_port);
	dest.sin_port = htons(dst_port);
	
	// IP addresses
	// We default source to 0.0.0.0 for several reasons, one, because bind() will fail if
	// this isnt a locallly accessbile IP, and two, it will seg fault if we use 
	// INADDY_ANY or INADDR_LOOPBACK
	source.sin_addr.s_addr = inet_addr("0.0.0.0"); 
	dummy.sin_addr.s_addr = inet_addr(sourceIP);
	dest.sin_addr.s_addr = inet_addr(destIP);	



	

	//bind our receiver socket.
	int r = bind(recv_sock, (struct sockaddr*)&source, sizeof(source));
	if( r != 0){
		perror("Bind Failed: ");
		return -1;
	}
    
    printf("UDP Socket is ready\n");
	//lets start the receiver thread
	//Prefere a detached thread, so no waiting
	if( pthread_attr_init( &attr) != 0 ){
		printf("Failed to initialize the pthread_attr\n");
		return -1;
	}
	int ss = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	thread_ret = pthread_create( &recv_thread,&attr, run_recv_thread, (void*)&dummy);
	if(thread_ret != 0 ){
		printf("DEBUG: Something happened when creating the thread!\n");
		return -1;
	}
	
	//printf("DEBUG: Thread is running with return value %d\n",thread_ret);
	int result;

	do{
		// !!!!nuclear reactive code here!!!!
		printf("Send a Message: [Enter to send] \n");
		fgets(s, DATA_LEN, stdin);
		if( strcmp(s,"quit") == 0){
			break;
		}

		//lets construct our manual packet
		int length = 0;
		buffer_raw_udp(s,strlen(s), data, &length);

		//send the data
		result = sendto(sock, data, length, 0, (struct sockaddr*)&dest, sizeof(dest) );
		if(result < 0)
		{
			printf("Error in sendto()\n");
			return -1;
		}
		//printf("[ %d ] bytes of data sent\n", result);
		sleep(2);

		memset(data, 0, PCK_LEN);

	}while(1);

	pthread_attr_destroy(&attr);

	close(sock);

	return 0;
}