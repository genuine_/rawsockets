                $$\                     $$\                  $$\     
                $$ |                    $$ |                 $$ |    
$$\   $$\  $$$$$$$ | $$$$$$\   $$$$$$$\ $$$$$$$\   $$$$$$\ $$$$$$\   
$$ |  $$ |$$  __$$ |$$  __$$\ $$  _____|$$  __$$\  \____$$\\_$$  _|  
$$ |  $$ |$$ /  $$ |$$ /  $$ |$$ /      $$ |  $$ | $$$$$$$ | $$ |    
$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |  $$ |$$  __$$ | $$ |$$\ 
\$$$$$$  |\$$$$$$$ |$$$$$$$  |\$$$$$$$\ $$ |  $$ |\$$$$$$$ | \$$$$  |
 \______/  \_______|$$  ____/  \_______|\__|  \__| \_______|  \____/ 
                    $$ |                                             
                    $$ |                                             
                    \__|  



Author: Greg Lindor


The way the program should be used is that both parties should know which 'port' is avialable to send 
traffic through, and their source and destination ports should reflect this, no issues in such a perfect case.

Usage:
Code compiled on Ubuntu flavored Linux. Linux Mint and Ubuntu 12.04 32bit

Compile both programs using gcc -pthread filename.c, 
then run with root privs ./filename [source host/ip] [source port] [destination host/ip] [destination port]

CTRL + C to quit

Sample usage:
./rawudp 0.0.0.0 26500 192.168.80.128 26500
./rawicmp 0.0.0.0 26500 192.168.80.128 26500

Sample Run:

root@ubuntu:/home/genuine/Downloads# ./rawudp 192.168.80.132 26500 xx.xxx.xx.xxx 26500
UDP Socket is ready
Send a Message: [Enter to send] hello my name is bob
Message Received: hello my name is bob

Send a Message: [Enter to send] oh hai
Message Received: oh hai

Send a Message: [Enter to send] ^C


